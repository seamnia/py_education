""" Проверка на то, что треды не блокируются на time.sleep
А вот треды в asyncio на sleep блокируются
"""
import asyncio
import concurrent.futures as con
import time
from functools import partial
from random import choice
from threading import Lock


def slow_func(lock, num):
    sleep = choice(range(1, 10))
    with lock:
        print("start, num =", num, "sleep =", sleep)
    time.sleep(sleep)
    with lock:
        print("finish, num =", num, "sleep =", sleep)


def main_thread():
    with con.ThreadPoolExecutor(6) as executor:
        partial_slow_func = partial(slow_func, lock)
        executor.map(partial_slow_func, range(5))


def main_asyncio():
    loop = asyncio.get_event_loop()
    for i in range(5):
        partial_slow_func = slow_func(lock, i)
        loop.run_in_executor(None, partial_slow_func)


def save_file():
    loop = asyncio.get_event_loop()
    for i in range(5):
        partial_slow_func = slow_func(lock, i)
        loop.run_in_executor(None, partial_slow_func)


if __name__ == "__main__":
    lock = Lock()
    start = time.time()
    main_thread()
    # main_asyncio()
    print("time: ", time.time() - start)
