"""Проверяем сохранение файлов через треды asyncio
START
run_consistently 8.808185815811157
run_async 6.378178596496582
run_in_threads 6.997225046157837
FINISH
"""

import asyncio
import os
import shutil
from concurrent.futures import ThreadPoolExecutor
from functools import partial, wraps

COUNT = 100
DIR = "files"
TEXT = str(list(range(9_000_000)))


def execution_time(fn):
    @wraps(fn)
    def wrapper():
        start = time.time()
        fn()
        print(fn.__name__, time.time() - start)

    return wrapper


def _recreate_dir():
    if os.path.exists(DIR):
        shutil.rmtree(DIR)
    os.mkdir(DIR)


def remove_dir(fn):
    @wraps(fn)
    def wrapper():
        _recreate_dir()
        fn()

    return wrapper


def write(i):
    with open(f"{DIR}/{i}.txt", "w") as file:
        file.write(TEXT)
    return True


async def run_async():
    _recreate_dir()
    loop = asyncio.get_event_loop()
    futures = []
    start = time.time()
    for i in range(COUNT):
        part = partial(write, i)
        futures.append(loop.run_in_executor(None, part))
    await asyncio.wait(futures)
    print("run_async", time.time() - start)


@execution_time
@remove_dir
def run_in_threads():
    with ThreadPoolExecutor(max_workers=5) as executor:
        executor.map(write, range(COUNT))


@execution_time
@remove_dir
def run_consistently():
    for i in range(COUNT):
        write(i)


if __name__ == "__main__":
    import time

    print("START")
    run_consistently()
    asyncio.run(run_async())
    run_in_threads()
    print("FINISH")
