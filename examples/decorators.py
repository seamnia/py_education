def counter(incr, attr):
    """ Декоратор, который уменьшает или увеличивает значение атрибута attr в классе
    Может использоваться как с корутинами, так и обычными функциями
    """

    def wrapper(fn):
        def _call_counter(self):
            assert hasattr(self, attr)
            count = getattr(self, attr)
            setattr(self, attr, count + incr)
            
        if inspect.iscoroutinefunction(fn):
            async def _counter(self, *args, **kwargs):
                _call_counter(self)
                return await fn(self, *args, **kwargs)
        else:
            def _counter(self, *args, **kwargs):
                _call_counter(self)
                return fn(self, *args, **kwargs)

        return wraps(fn)(_counter)

    return wrapper
